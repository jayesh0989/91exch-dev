<?php
namespace App\Http\Controllers\GameOver;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use stdClass;


class DataCleanController extends Controller
{

    public function redisClean(Request $request){
        $cache = Redis::connection();
        $cache->flushall();
        echo "Done";

        // Admin Block Event
        $eventIds = [];
        $eventBlockArr = DB::table('tbl_block_event_market')->get();
        if( $eventBlockArr->isNotEmpty() ){
            foreach ($eventBlockArr as $blockArr){
                $eventIds[] = $blockArr->eid;
            }
        }
        $cache->set('Block_Event_Market',json_encode($eventIds));

        // Admin Bet Allowed Event
        $eventIds2 = [];
        $eventData = DB::table('tbl_bet_allowed_event')->get();
        if( $eventData->isNotEmpty() ){
            foreach ($eventData as $event){
                $eventIds2[] = $event->eid;
            }
        }
        $cache->set('Bet_Allowed_Events',json_encode($eventIds2));
    }

    public function PendingResult(){
        try {
            $marketIds = null;
            $url = 'https://fawk.app/api/exchange/odds/market/result';
            // Current date and time
            $datetime = date("Y-m-d H:i:s");
            // echo "current date-->".$datetime; echo "<br>";
            // Convert datetime to Unix timestamp
            $timestamp = strtotime($datetime);
            // Subtract time from datetime
            $time = $timestamp - (1 * 60 * 30);
            // Date and time after subtraction
            $datetime = date("Y-m-d H:i:s", $time);
            // echo "convert date-->".$datetime; exit;
            $query = DB::connection('mongodb')->table('tbl_bet_pending_teenpatti')
                ->where([['result', 'PENDING'], ['status', 1],['sid',99]])->where([['created_on', '<=', $datetime]]);
            $betList = $query->distinct()->select('mid')->get();
            //  echo "<pre>"; print_r($betList);exit;
            if (!empty($betList)) {

                $settingData = DB::table('tbl_common_setting')->select('key_name', 'value')
                    ->where('key_name','TEENPATTI_OPERATORID')
                    ->first();
                $operatorId =$settingData->value;

                // print_r($betList); exit;
                foreach ($betList as $data) {
                    if (isset($data->mid)) {

                        $marketId = $data->mid;
                        $transactionCheck = DB::connection('mysql3')->table('tbl_transaction_client')->select(['mid'])
                            ->where([['mid', $marketId]])->first();
                        if ($transactionCheck != null) {
                            // do nothing
                        }else {
                            $resultCheck = DB::connection('mongodb')->table('tbl_teenpatti_result')->select(['user_data'])
                                ->where([['mid', $marketId]])->first();
                            if ($resultCheck != null) {
                                // do nothing
                            } else {
                                $marketIds[] = $marketId;
                            }
                        }
                    }
                }

                if ($marketIds != null && !empty($marketIds)) {
                    //$marketIds=json_encode($marketIds);
                    $post = [
                        'operatorId' => $operatorId,
                        'markets' => $marketIds
                    ];

                    // print_r($post); exit;
                    $post = json_encode($post);
                    //  print_r($post); exit;
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $response = curl_exec($ch);
                    curl_close($ch);

                    // print_r($response);
                    echo "done.";
                    //exit;
                }
            }


            $this->TeenpattiExposeUpdate();

            echo "<pre>"; print_r($marketIds); exit;


        }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
    }


    public function TeenpattiExposeUpdate()
    {

        try {

            $datetime = date("Y-m-d H:i:s");
            // echo "current date-->".$datetime; echo "<br>";
            // Convert datetime to Unix timestamp
            $timestamp = strtotime($datetime);
            // Subtract time from datetime
            $time = $timestamp - (1 * 60 * 300);
            // Date and time after subtraction
             $datetime = date("Y-m-d H:i:s", $time);

            $List = DB::connection('mongodb')->table('tbl_bet_pending_teenpatti')->select('*')->where([['created_on', '<', $datetime], ['result', 'PENDING'], ['status', 1],['sid',99]])->orderBy('id', 'asc')->get();
            //echo "<pre>"; print_r($List); exit;
            if (!empty($List)) {
                foreach ($List as $betlist) {
                    $id = $betlist->id;
                    $userId = $betlist->uid;
                    //exit;
                    $marketId = $betlist->mid;
                    $eventId = $betlist->eid;
                    // Update User Event Expose
                    DB::connection('mongodb')->table('tbl_user_market_expose')
                        ->where([['uid', $userId], ['mid', $marketId]])->update(['status' => 2]);

                    DB::connection('mongodb')->table('tbl_bet_pending_teenpatti')->where([['uid', $userId], ['mid', $marketId]])->update(['result' => 'CANCELED']);

                    $balExpose = $expose = 0;
                    $userMarketExpose = DB::connection('mongodb')->table('tbl_user_market_expose')->select('expose')
                        ->where([['uid', $userId], ['status', 1]])->get();
                    if ($userMarketExpose->isNotEmpty()) {
                        foreach ($userMarketExpose as $data) {
                            $data = (object)$data;
                            if ((int)$data->expose > 0) {
                                $expose = $expose + (int)$data->expose;
                            }
                        }
                        $balExpose = round($expose);
                    }

                    $updated_on = date('Y-m-d H:i:s');
                    $updateData = [
                        'expose' => $balExpose,
                        'updated_on' => $updated_on
                    ];

                    DB::connection('mysql2')->table('tbl_user_info')->where('uid', $userId)->update($updateData);
                }
            }


            echo "Done.";
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
    }


}
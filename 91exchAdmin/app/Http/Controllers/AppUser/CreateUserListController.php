<?php

namespace App\Http\Controllers\AppUser;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

class CreateUserListController extends Controller
{
    public function userList(){
        $user = Auth::user();
        $user = $user->roleName;

        $userArr = [['id' =>1,'name' => 'Sub Admin'],['id'=>2,'name'=>'Super Master'],['id'=>3,'name' => 'Master'],['id'=> 4,'name' =>'Clinet'],['id'=>5,'name'=> 'Supervisor'], ['id'=>6,'name'=>'Super Admin']];
        
        if(in_array($user,['ADMIN'])){
            unset($userArr[3]);
        }
        elseif(in_array($user,['ADMIN2'])){
            unset($userArr[0]);unset($userArr[3]);unset($userArr[4]);unset($userArr[5]);
        }
        elseif(in_array($user,['SM1']) ){
            unset($userArr[0]);unset($userArr[4]);unset($userArr[5]);
        }
        elseif(in_array($user,['SM2']) ){
            unset($userArr[0]);unset($userArr[4]);unset($userArr[5]);unset($userArr[1]);
        }
        elseif(in_array($user,['SA','SV']) ){
            $userArr = [];
        }
        elseif(in_array($user,['M1']) ){
            unset($userArr[0]);unset($userArr[2]);unset($userArr[1]);unset($userArr[4]);unset($userArr[5]);
        }
        else{
            $userArr = 'Not valid user';
        }


        $response = ['status' => 1, 'data' => array_values($userArr)];
        return $response;
    }
}

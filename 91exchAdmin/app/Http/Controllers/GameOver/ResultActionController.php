<?php

namespace App\Http\Controllers\GameOver;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class ResultActionController extends Controller
{
    public function gameRecallAction(Request $request){
        $validate = Validator::make($request->all(),[           
                                                        'eventId' =>'required',
                                                        'marketId' =>'required',
                                                    ]);
        if($validate->fails()){
            return response()->json(['status' => 1, 'success' => ["message" => $validate->errors()]]);
        }
        $eventId  = $request->get('eventId',0);
        $marketId = $request->get('marketId',0);

        $resultData = DB::connection('mongodb')->table('tbl_market_result')->where([['eventId',$eventId],['marketId',$marketId]])->first();
        if(!empty($resultData)){

            $recalldata["sportId"]    = isset($resultData['sportId'])?$resultData['sportId']:null;
            $recalldata["eventId"]    = isset($resultData['eventId'])?$resultData['eventId']:null;
            $recalldata["marketId"]   = isset($resultData['marketId'])?$resultData['marketId']:null;
            $recalldata["secId"]      = isset($resultData['secId'])?$resultData['secId']:null;
            $recalldata["mType"]      = isset($resultData['mType'])?$resultData['mType']:null;
            $recalldata["winner"]     = isset($resultData['winner'])?$resultData['winner']:null;
            $recalldata["game_over"]  = 1;
            $recalldata["recall"]     = 1;
            $recalldata["status"]     = 1;
            $recalldata["created_on"] = date('Y-m-d h:i:s');
            $recalldata["updated_on"] = date('Y-m-d h:i:s');
            $recalldata["admin_recall_status"] = 1;
            
            //to soft delet old record
            DB::connection('mongodb')->table('tbl_market_result')->where([['eventId',$eventId],['marketId',$marketId]])->update(['status'=>2]);

            //create new record for recall
            $createGameRecall = DB::connection('mongodb')->table('tbl_market_result')->insert($recalldata);
            if($createGameRecall)
            { 
                return response()->json(['status' => 1, 'success' => ["message" => "Game recall successfully done."]]);
            }
            else
            {
                return response()->json(['status' => 0, 'success' => ["message" => "Game recall failed."]]);
            }
        }
        else
        {
            return response()->json(['status' => 0, 'success' => ["message" => "Record Not Found."]]);
        }
    }

    public function gameOverAction(Request $request){
        $validate = Validator::make($request->all(),[           
                                                        'eventId' =>'required',
                                                        'marketId' =>'required',
                                                        'winResult' =>'required',
                                                    ]);
        if($validate->fails()){
            return response()->json(['status' => 1, 'success' => ["message" => $validate->errors()]]);
        }

        $secId=0;
        $eventId   = $request->get('eventId',0);
        $marketId  = $request->get('marketId',0);
        $winResult = $request->get('winResult',"");
        $winner    = $request->get('winner',"");
        if(!empty($winResult)){
            $winnerString = explode('_', $winResult);
            $winner = isset($winnerString[1])?$winnerString[1]:$winResult;
            if(isset($winnerString[0])){
                $secId=$winnerString[0];
            }
        }

        /*echo "winResult-->".$winResult;
        echo "<br>";
        echo "winner-->".$winner;
        exit;*/
        $resultData = DB::connection('mongodb')->table('tbl_market_result')
                                                ->where([['eventId',$eventId],['marketId',$marketId],['status',1]])
                                                ->first();
        if(!empty($resultData)){

            $gameOverdata["sportId"]    = isset($resultData['sportId'])?$resultData['sportId']:null;
            $gameOverdata["eventId"]    = isset($resultData['eventId'])?$resultData['eventId']:null;
            $gameOverdata["marketId"]   = isset($resultData['marketId'])?$resultData['marketId']:null;
            $gameOverdata["secId"]      = $secId;
            $gameOverdata["mType"]      = isset($resultData['mType'])?$resultData['mType']:null;
            $gameOverdata["winner"]     = $winner;
            $gameOverdata["game_over"]  = 0;
            $gameOverdata["recall"]     = 0;
            $gameOverdata["status"]     = 1;
            $gameOverdata["created_on"] = date('Y-m-d h:i:s');
            $gameOverdata["updated_on"] = date('Y-m-d h:i:s');
            $gameOverdata["admin_recall_status"] = 0;
            //to soft delet old record
            DB::connection('mongodb')->table('tbl_market_result')->where([['eventId',$eventId],['marketId',$marketId]])->update(['status'=>2]);
            //create new record for game over
            
            $gameOver = DB::connection('mongodb')->table('tbl_market_result')->insert($gameOverdata);
           // dd($gameOver);
            if($gameOver)
            { 
                return response()->json(['status' => 1, 'success' => ["message" => "Game over successfully done."]]);
            }
            else
            {
                return response()->json(['status' => 0, 'success' => ["message" => "Game over failed."]]);
            }
        }
        else
        {
            return response()->json(['status' => 0, 'success' => ["message" => "Record Not Found."]]);
        }
    }

    public function gameAbundantAction(Request $request)
    {
        try {
            $validate = Validator::make($request->all(), [
                'eventId' => 'required',
                'marketId' => 'required',
            ]);
            if ($validate->fails()) {
                return response()->json(['status' => 1, 'success' => ["message" => $validate->errors()]]);
            }
            $eventId = $request->get('eventId', 0);
            $marketId = $request->get('marketId', 0);

            $resultData = DB::connection('mongodb')->table('tbl_market_result')->where([['eventId', $eventId], ['marketId', $marketId], ['status', 1]])->first();
            if (!empty($resultData)) {

                $recalldata["sportId"] = isset($resultData['sportId']) ? $resultData['sportId'] : null;
                $recalldata["eventId"] = isset($resultData['eventId']) ? $resultData['eventId'] : null;
                $recalldata["marketId"] = isset($resultData['marketId']) ? $resultData['marketId'] : null;
                $recalldata["secId"] = 0;
                $recalldata["mType"] = isset($resultData['mType']) ? $resultData['mType'] : null;
                $recalldata["winner"] = 'ABUNDANT';
                $recalldata["runners"] = isset($resultData['runners']) ? $resultData['runners'] : '';
                $recalldata["game_over"] = 0;
                $recalldata["recall"] = 2;
                $recalldata["status"] = 1;
                $recalldata["created_on"] = isset($resultData['created_on']) ? $resultData['created_on'] : '';
                $recalldata["updated_on"] = date('Y-m-d h:i:s');

                //to soft delet old record
                DB::connection('mongodb')->table('tbl_market_result')->where([['eventId', $eventId], ['marketId', $marketId]])->update(['status' => 2]);

                //create new record for recall
                $createGameAbundunt = DB::connection('mongodb')->table('tbl_market_result')->insert($recalldata);
                if ($createGameAbundunt) {
                    return response()->json(['status' => 1, 'success' => ["message" => "Game Abundant successfully done."]]);
                } else {
                    return response()->json(['status' => 0, 'success' => ["message" => "Game recall failed."]]);
                }
            } else {
                return response()->json(['status' => 0, 'success' => ["message" => "Record Not Found."]]);
            }
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
    }
}

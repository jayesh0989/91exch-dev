<?php
namespace App\Http\Controllers\Market;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
class MarketResultController extends Controller
{
  public function marketResult(Request $request)
  {
    try{
        $response = [ "status" => 0 , "code" => 400 , "message" => "Bad request!" ];
        $arr      = [];

        // if( json_last_error() == JSON_ERROR_NONE ){
        //   if( isset( $request->start_date ) && isset( $request->end_date )
        //     && ( $request->start_date != '' ) && ( $request->end_date != '' )
        //     && ( $request->start_date != null ) && ( $request->end_date != null ) ){

        //     $startDate = date('Y-m-d', strtotime($request->start_date));
        //     $startDate = $startDate." 00:00:01";     
        //     $endDate   = date('Y-m-d', strtotime($request->end_date));
        //     $endDate   = $endDate." 23:59:59";
        //   }else{
        //     $start     = new \DateTime('now +1 day');
        //     $endDate   =  $start->format('yy-m-d h:i:s');
        //     $end       = new \DateTime('now -7 day');
        //     $startDate = $end->format('yy-m-d h:i:s');  
        //   }
        // }
        
        $query = DB::connection('mongodb')->table('tbl_market_result')
                                          ->select('*')
                                          ->where([['status',1]])
                                          ->where('result','!=','null')
                                          ->orderBy('created_on','DESC');
        $now        = Carbon::now();
        $endDate    = $now->format('Y-m-d');
        if(isset($request->ftype) && $request->ftype == 'week')
        {
          $startDate  = $now->subDays(7)->format('Y-m-d');
          $marketList = $query->whereBetween('created_on',[$startDate, $endDate])->get();
        }
        else
        {
          $marketList = $query->limit(100)->get();
        }

        if(!$marketList->isEmpty())
        {
          foreach ($marketList as $key=>$marketData) 
          {

              $tbl = 'tbl_market_data'; $runners=null;
              $where = [['eventId',$marketData['eventId']],['marketId',$marketData['marketId']]];
              $resultData = DB::connection('mongodb')->table($tbl)->where($where)->first();
            if (isset($marketData->mType) && $marketData->mType == 'casino') {
              $desc        = explode(">", $marketData->description);
              $description = $desc[0].'>'.$desc[1].'>'.$desc[2];
              $result      = $marketData->winner;
            }else{
                 $description = $resultData['event_name']." -> ".$resultData['market_name'];
                $mData = json_decode($resultData['mData']);
                $runners = isset($mData->runners)?json_encode(json_decode($mData->runners)):'';
            }


              $admin_recall_status=0;
              if(isset($marketData['admin_recall_status']) && $marketData['admin_recall_status']==1){
                  $admin_recall_status=1;
              }

            $arr [] = [
                        'eventId'     => isset($marketData['eventId'])?$marketData['eventId']:'N/A',
                        'marketId'    => isset($marketData['marketId'])?$marketData['marketId']:'N/A',
                        'eventName'    => isset($resultData['event_name'])?$resultData['event_name']:'N/A',
                        'marketName'    => isset($resultData['market_name'])?$resultData['market_name']:'N/A',
                        'description' => $description,
                        'result'      => isset($marketData['winner'])?$marketData['winner']:'N/A',
                        'date'        => isset($marketData['updated_on'])?$marketData['updated_on']:'N/A',
                        'game_over'   => isset($marketData['game_over'])?$marketData['game_over']:'N/A',
                        'recall'   => isset($marketData['recall'])?$marketData['recall']:'0',
                        'runners'    => $runners,
                        'showRunner' => (!empty($runners))? 1 :0,
                        'admin_recall_status'=>$admin_recall_status
                      ];
          }
          $response = [ "status" => 1 ,'code'=> 200, "data" => $arr ,'message'=> 'Data Found !!' ];
        }
        else
        {
         $response = [ "status" => 1 ,'code'=> 200, "data" => [] ,'message'=> 'Data Not Found !!' ];
        }
        return $response;
    }catch (\Exception $e) {
      $response = $this->errorLog($e);
      return response()->json($response, 501);
    }
  } 
}

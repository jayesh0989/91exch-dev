<?php

namespace App\Http\Controllers\History;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class ChipHistoryController extends Controller
{

    /**
     * Chip History List
     */
    public function list($uid = null)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{

            if( $uid != null ){
                if( $uid == 1 && Auth::id() != 1 ){ return response()->json($response); }
                $user = DB::table('tbl_user')->where([['id',$uid],['status',1]])->first();
            }else{
                $user = Auth::user(); $uid = $user->id;
                if( $user->role == 6 ){ $uid = 1; }
            }

            $requestData = json_decode( file_get_contents('php://input') , JSON_FORCE_OBJECT );
            $searchDate = false; $start = $end = null;
            if( !isset( $requestData[ 'ftype' ] ) && isset( $requestData[ 'start' ] ) && isset( $requestData[ 'end' ] ) ) {
                $start = Carbon::parse($requestData[ 'start' ]);
                $end = Carbon::parse($requestData[ 'end' ]);
                $searchDate = true;
            }

            if( isset( $requestData[ 'ftype' ] ) && $requestData[ 'ftype' ] != 'wl' ) {
                $now = Carbon::now();
                if( $requestData[ 'ftype' ] == 'week' ){
                    $end = $now->format('Y-m-d');
                    $start = $now->subDays(7)->format('Y-m-d');
                }elseif ( $requestData[ 'ftype' ] == 'month' ){
                    $end = $now->format('Y-m-d');
                    $start = $now->subDays(30)->format('Y-m-d');
                }else{
                    $end = $start = $now->format('Y-m-d');
                }

                $start = Carbon::parse($start); $end = Carbon::parse($end);
                $searchDate = true;
            }

            if( $user != null ){
                $userName = $user->name;
                if( $user->role == 1 || $user->role == 6 ){
                    $tbl = 'tbl_transaction_admin_view';
                }elseif ( $user->role == 4 ){
                    $tbl = 'tbl_transaction_client_view';
                }else{
                    $tbl = 'tbl_transaction_parent_view';
                }

                if( isset( $requestData[ 'ftype' ] ) && $requestData[ 'ftype' ] == 'wl' ) {
                    $query = DB::connection('mysql3')->table($tbl)
                        ->select(['description','balance','remark','updated_on','amount','type'])
                        ->where([['userId',$uid],['status',1],['eType',1],['systemId','!=',1]]);
                }else{
                    $query = DB::connection('mysql3')->table($tbl)
                        ->select(['description','balance','remark','updated_on','amount','type'])
                        ->where([['userId',$uid],['status',1],['eType',1],['systemId',$user->systemId]]);

                    if( $searchDate == true && $start != null && $end != null ){
                        $query->whereDate('updated_on','<=',$end->format('Y-m-d'))
                            ->whereDate('updated_on','>=',$start->format('Y-m-d'));
                    }else{
                        $now = Carbon::now();
                        $end = $start = $now->format('Y-m-d');
                        $start = Carbon::parse($start); $end = Carbon::parse($end);
                        $query->whereDate('updated_on','<=',$end->format('Y-m-d'))
                            ->whereDate('updated_on','>=',$start->format('Y-m-d'));
                    }
                }

                $list = $query->orderBy('updated_on', 'DESC')->get();

                if( $list->isNotEmpty() ){
                    $response = [ 'status' => 1, 'data' => $list, 'userName' => $userName ];
                }else{
                    $response = [ 'status' => 1, 'data' => [], 'userName' => $userName ];
                }
            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

}
